### Berserk CMS  
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/berserk/blob/master/LICENSE) [![Build Status](https://travis-ci.com/huaiping/berserk.svg?branch=master)](https://travis-ci.com/huaiping/berserk) [![Coverage Status](https://coveralls.io/repos/github/huaiping/berserk/badge.svg?branch=master)](https://coveralls.io/github/huaiping/berserk?branch=master)  
Open Source CMS. Just for practice.

### Requirements
OpenJDK 8+ [https://openjdk.java.net](https://openjdk.java.net)  
Tomcat 8.5+ [https://tomcat.apache.org](https://tomcat.apache.org)  
MySQL 5.5+ [https://www.mysql.com](https://www.mysql.com)

### Features
HTML 5 + Struts 2.5.20 + Spring 5.1.4 + Hibernate 5.4.0

### Demo
[https://www.huaiping.net](https://huaiping.net/v2/)

### License
Licensed under the [MIT License](https://gitlab.com/huaiping/berserk/blob/master/LICENSE).
