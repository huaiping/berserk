package berserk;

import net.huaiping.HibernateUtil;
import net.huaiping.UserDAO;
import net.huaiping.UserDAOImpl;
import net.huaiping.User;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
 
public class UserTest {
 
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
 
    @Before
    public void setUp() throws Exception {
    }
 
    @Test
    public void testSave() {
        UserDAO userDAO = new UserDAOImpl();
        try{
            User u = new User();
            u.setId(20);
            u.setName("zhangsan");
            u.setPassword("123456");
            u.setType("admin");
            userDAO.save(u);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void QueryTest() {
        Session session = HibernateUtil.getsSession();
        Transaction tx = session.beginTransaction();
        try {
            String hql = "from User";
            Query<User> query = session.createQuery(hql, User.class);
            List<User> list = query.list();
            for (User user : list) {
                System.out.println(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            HibernateUtil.closeSession();
        }
    }

}
