package net.huaiping;

import net.huaiping.HibernateUtil;
import net.huaiping.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
 
public class UserDAOImpl implements UserDAO {
 
    // 添加用户
    @Override
    public void save(User user) {
        Session session = HibernateUtil.getsSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(user);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            HibernateUtil.closeSession();
        }
    }
 
    // 根据id查找用户
    @Override
    public User findById(int id) {
        User user = null;
        Session session = HibernateUtil.getsSession();
        Transaction tx = session.beginTransaction();
        try {
            user = (User) session.get(User.class, id);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            HibernateUtil.closeSession();
        }
        return user;
    }
 
    // 删除用户
    @Override
    public void delete(User user) {
        Session session = HibernateUtil.getsSession();
        Transaction tx = session.beginTransaction();
        try {
            session.delete(user);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            HibernateUtil.closeSession();
        }
    }
 
    // 修改用户信息
    @Override
    public void update(User user) {
        Session session = HibernateUtil.getsSession();
        Transaction tx = session.beginTransaction();
        try {
            session.update(user);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            HibernateUtil.closeSession();
        }
    }
 
}
